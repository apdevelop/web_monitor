
# Process single image using the run() method. Note that all related files are based on the full
# path of a given image.


import sys
import os
from time import sleep

import cv2
import numpy as np


import numpy as np
import cv2
from matplotlib import pyplot as plt

import mail_service

from cfg import *


import socket




reportedIncidents = {}; # used to avoid multiple reports per incident



def run( imgPath, iconList, sendMail):

	scaleImgPath = imgPath[0:-3] + "scale.png";
	cropImgPath = imgPath[0:-3] + "crop.png";


	img = cv2.imread(imgPath)


	#--- mask  TODO


	#--- resize - no needed...
	# res = cv2.resize(img,None,fx=2, fy=2, interpolation = cv2.INTER_CUBIC)

	# cv2.imwrite(scaleImgPath,res)




	#--- crop - not needed...

	# crop = res[200:1700, 100:1900] # Crop from x, y, w, h -> 100, 200, 300, 400
	# # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]

	# cv2.imwrite(cropImgPath, crop)


	#--- match!

	for icon in iconList:
		findMatch(imgPath, icon, sendMail)



def findMatch(imgPath, icon, sendMail):

	searchedImgPath = "icons/" + icon + ".png"
	img1 = cv2.imread(imgPath,0)          # queryImage
	img2 = cv2.imread(searchedImgPath,0) # trainImage

	grade = 0

	# Initiate SIFT detector
	sift = cv2.xfeatures2d.SIFT_create()

	# find the keypoints and descriptors with SIFT
	kp1, des1 = sift.detectAndCompute(img1,None)
	kp2, des2 = sift.detectAndCompute(img2,None)

	# BFMatcher with default params
	bf = cv2.BFMatcher()
	matches = bf.knnMatch(des1,des2, k=2)

	# Apply ratio test
	good = []

	incidentId = ""
	for m,n in matches:
		if m.distance < 0.4*n.distance:
			#print("Debug: Found some match" + "  m.distance= " + str(m.distance) + "   n.distance= " + str(n.distance) )

			incidentId += "[" + str(int(m.distance)) + "," + str(int(n.distance)) +  "] "
			grade += 1
			good.append([m])

	#print("Debug: incidentId=", incidentId)


	if (grade > 0) and (incidentId not in reportedIncidents):

		reportedIncidents[incidentId] = True 

		img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=2)

		matchImgPath = imgPath[0:-3] + icon + ".MATCH__" + str(grade) + ".png";

		cv2.imwrite(matchImgPath, img3)

		#--- send email
		if (sendMail):


			subject = "Incident report - " + icon 
			message = "Found " + icon + " in " + imgPath + "\n\n" + "Result can be found on " + \
			socket.gethostname() + " in: " + os.path.realpath(matchImgPath) + "\n"


			mail_service.sendEmail(subject, message, matchImgPath)

			# temporal log
			fo = open("mail_messages.log", "a")
			fo.write("\n\n==========================\n" + subject + "\n" + message)
			fo.close()




















