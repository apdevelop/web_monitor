

# send email based on config parameters and sendEmail() function


from cfg import *


import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
 
from time import sleep





def sendEmail(subject, message, png=""):

	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromAddr, password)


	msg = MIMEMultipart()
	msg['From'] = fromAddr
	msg['To'] = ", ".join(recipients)
	msg['Subject'] = subject
	 
	body = message
	msg.attach(MIMEText(body, 'plain'))
	 
	if (png != ""):
		with open(png, 'rb') as fp:
			img = MIMEImage(fp.read())
			msg.attach(img)


	text = msg.as_string()

	print("Sending mail with sibject: ", subject, "   to: ", recipients)
	server.sendmail(fromAddr, recipients, text)
	
	server.quit()

	



