
Usage: python3.5 webrun -h
=====

Example: python3.5 webrun.py -rate 300 -iter 100 -section north -icons accident:police 
=======


Dependencies: python 3.5, selenium, chromium, chromdriver, opencv3 
============

Dependencies install details:
============================

# python 
brew install python3

# selenium
pip3.5 install selenium

# chromium
Install chromium based on (https://download-chromium.appspot.com/)

# chromdriver
Install latest chromdriver
Note: You will need to set path to chromedriver in cfg.py

# opencv
# based on: https://gravityjack.com/news/opencv-python-3-homebrew/
brew reinstall opencv3 --with-python3 --c++11 --with-contrib
brew unlink opencv
brew link --force opencv3







