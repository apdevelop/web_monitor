

# Top program to contnuesly run waze live, sample screen and search for icons
# Note that all configuration should be done in cfg.py


from time import sleep
from selenium import webdriver

import process_image
 
import sys
import os

import argparse

import time

import mail_service

from cfg import *

import socket

import atexit





#--- user args
parser = argparse.ArgumentParser()

parser.add_argument("-section", type=str, default=sectionDef, help="Road section name. Default: " + str(sectionDef))
parser.add_argument("-rate", type=int, default=sleepTimeDef, help="sample rate in seconds. Default: " + str(sleepTimeDef))
parser.add_argument("-iter", type=int, default=loopNumDef, help="number of iterations. Default: " + str(loopNumDef))
parser.add_argument("-icons", type=str, default=iconDef, 
	help="search icons from icons/* (seperated by ':').  Default: " + str(iconDef))

parser.add_argument("--no_mail", dest='send_mail', action='store_false', help="don't send mail" )

parser.add_argument("--no_exit_mail", dest='send_mail_on_exit', action='store_false', 
	help="don't send mail in case of program exit (for testing)" )


args = parser.parse_args()

iconList = args.icons.split(":")




#---------------------------------------------------
def reportProgramExit():
	print("===============>>>> In reportProgramExit")
	subject = "Job webrun.py on " + socket.gethostname() + " exited!"
	message = ""
	mail_service.sendEmail(subject, message)


if (args.send_mail_on_exit):
	atexit.register(reportProgramExit)
#-----------------------------------------------------




driver = webdriver.Chrome(chromeDriverPath)  


#--- main

driver.get(url);



# wait for user to zoom in as needed
userResponse = input("Prepare your screen (location & resolution) and click 'Enter' when ready...")

count = 0
while (count < args.iter):
	
	#--- create out-dir 
	dateStr =  time.strftime("%Y-%m-%d", time.localtime(time.time())) 
	hourStr =  time.strftime("%H", time.localtime(time.time())) 

	outDir = outMainDir + '/' + args.section + '/' + dateStr + '/' + hourStr


	try:
		os.makedirs(outDir, exist_ok=True)
	except IOError:
	   print ("Error: can\'t create directory " + outDir )




	timeStr =  time.strftime("%H-%M", time.localtime(time.time())) 
	fileNmae = outDir + '/live_waze.'  + timeStr + '.png'


	print("Saving screenshot to " + fileNmae)



	driver.save_screenshot(fileNmae)

	# start process it
	process_image.run(fileNmae, iconList, args.send_mail)

	count = count + 1
	
	sleep(args.rate)



driver.quit()










