


#--- Configuration and defaults


url = 'https://www.waze.com/he/livemap'

chromeDriverPath = '/Users/avipuder/Downloads/chromedriver'   # must be updated on your machine !

sleepTimeDef = 300          # delay between iterations


loopNumDef = 2500 		   # number of iterations 

sectionDef = "def_section" # road section  

iconDef = "accident"       # what icon we are searching for (one of icons/ directory)

mailDef = True             # send mail by default?

outMainDir = 'screen_images'; # root of output directory


#--- mail  
fromAddr = "waycare.incident.report"  # dedicated gmail account
password = "wayincrep"

recipients = ['avi1p1@gmail.com', 'idan@waycare-smart-highway.com', 'shai@waycare-smart-highway.com']   # can have multiple recipients


